#include "adjacencylist.h"

//Utility Function to Add Edges to our node tree
node_t * addEdge( node_t * start, int end, int weight){

//Initialize Memory Space of Next Node to zero
node_t * Node = (node_t *) calloc(1,sizeof(node_t));
//Set Parameters
Node->vertex = end;
Node->weight = weight;

Node->next=start;

return Node;
}

//organize a heap containing our cumulative distance
//traveled down nodes
void heap(vertex_t Heap[], int size, int check, int pos[]){
	
	//Temp Varible for swapping.
	vertex_t temp;
	
	
	//While the Left Side of the heap even exists
	while((2*check) < size){
		//If there's no right side child
		if((2*check)+1 > size){
			//Check if the Child is Smaller
			if(Heap[check].dist > Heap[2*check].dist){
				
				
				temp = Heap[check];
				Heap[check] = Heap[2*check];
				Heap[2*check] = temp;
				
				pos[Heap[check].vertex] = 2*check;
				pos[Heap[2*check].vertex] = check;
				
			}
			break; 
			// stop iterating here, 
			//It means you've reached the end of the largest child.
		}
			
		//If there's both and left and right child
		//And if one of the children is Smaller
		if(Heap[check].dist > Heap[check*2].dist || 
			Heap[check].dist > Heap[check*2+1].dist){
				
			if(Heap[check*2].dist <= Heap[check*2+1].dist){
				//Left Child is Lesser Than Parent, Swap.
				temp = Heap[2*check];
				Heap[2*check] = Heap[check];
				Heap[check] = temp;
				
				pos[Heap[check].vertex] = 2*check;
				pos[Heap[2*check].vertex] = check;
				//Iterate Down the Heap
				check = 2*check;
			}
			else{
				//Right Child Is Lesser than parent, Swap.
				temp = Heap[2*check+1];
				Heap[2*check+1] = Heap[check];
				Heap[check] = temp;
				
				pos[Heap[check].vertex] = 2*check+1;
				pos[Heap[2*check+1].vertex] = check;
				check = 2*check+1;
			}	
		}
		else{
			break;
		}
	}
}


//decrease key command. Rebuilds the heap and pushes the
//smallest value to the top of the stack.
void decreaseKey(vertex_t Heap[], vertex_t Node, int pos[]){
	
	int i = pos[Node.vertex];
	vertex_t temp;

	Heap[i].dist = Node.dist;
	
	while(i>0){
		//Check if Child is greater than Parent
		if(Heap[i/2].dist > Heap[i].dist){
			pos[Heap[i].vertex] = i/2;
			pos[Heap[i/2].vertex] = i;
			
			temp = Heap[i/2];
			Heap[i/2] = Heap[i];
			Heap[i] = temp;
			i = i / 2;
		}
		else{
			break;
		}

	}


}
//Function to get the smallest value
//With the heap, should be at the head of the heap.
vertex_t pop(vertex_t Heap[], int size, int pos[]){
	pos[Heap[0].vertex] = size-1;
	pos[Heap[size-1].vertex] = 0;
	
	vertex_t min=Heap[0];
	Heap[0] = Heap[size-1];
	--size;
	heap(Heap,size,0,pos);
	return min;
}


//Construct the heap.
void buildHeap(vertex_t Heap[], int size, int pos[]){
	int i;
	
	for(i = (size/2);i>0;--i){
		heap(Heap,size,i,pos);
	}

}