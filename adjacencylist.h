#ifndef ADJACENCYLIST_H
#define ADJACENCYLIST_H
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>


//Used to Build Edges and Node Tree
typedef struct node_t{
	int vertex;
	int weight;
	struct node_t * next;
} node_t;

typedef struct vertex_t
{
	int vertex;
	int dist;
} vertex_t;

//Add Function Prototypes
node_t * addEdge( node_t * start, int end, int weight);
void heap(vertex_t Heap[], int size, int check, int pos[]);
void decreaseKey(vertex_t Heap[], vertex_t Node, int pos[]);
vertex_t pop(vertex_t Heap[], int size, int pos[]);
void buildHeap(vertex_t Heap[], int size, int pos[]);


#endif
