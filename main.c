#include "dungeon.h"






int main(int argc, char * argv[]){

  int i;
  bool save = false;
  bool load = false;
  
  int Max_Rooms = 5;
  int rockhardnessmap[MAX_H][MAX_V];
  char * filename = malloc(50); //Guarantee Memory Space
  filename = "dungeon";
  
  for(i = 1;i<argc;i++){
    if(!strcmp(argv[i],"--save")){
      save = true;
    }
    else if(!strcmp(argv[i],"--load")){
      load = true;
    }
    else if(!strcmp(argv[i],"-s")){
      if(i+1<argc){
	Max_Rooms = atoi(argv[i+1]) > 5 ? atoi(argv[i+1]) : 5;
      } 
    }    
    else if(!strcmp(argv[i], "-l")){
      if(i+1<argc){
	filename = argv[i+1];
	printf("%d\n", !strcmp(argv[i+1],""));
	if(filename[0] != '-' && strcmp(argv[i+1],"")){ 
	  filename = argv[i+1];
	}
	else{
	  printf("Invalid Load File, using dungeon\n");
	  filename = "dungeon";
	}
      }   
    }
    
  }
  
  room Rooms[5000];
   
  
if(!load){
  printf("Creating Dungeon\n");
  Max_Rooms = createDungeon(Max_Rooms,Rooms,rockhardnessmap);
}
else{

  printf("Loading Dungeon\n");
  int testMax_Rooms = loadDungeon(filename, rockhardnessmap,Rooms);
  if(testMax_Rooms == -1){
    testMax_Rooms = Max_Rooms;
    printf("Creating Dungeon\n");
    Max_Rooms = createDungeon(testMax_Rooms,Rooms,rockhardnessmap);
  }
  else{
  
    Max_Rooms = testMax_Rooms;
  }
  
}

if(save){
  printf("Saving Dungeon\n");  
  saveDungeon(filename, rockhardnessmap,Rooms, Max_Rooms);  
  
}

return 0;
}