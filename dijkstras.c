#include "dijkstras.h"


pList dikstras(int map[MAX_H][MAX_V], int start, int end){


int i,j;
int total_vertex = MAX_H*MAX_V;
node_t * aList[total_vertex];

//Set our Node List to Null
for(i=0;i<total_vertex;i++){
	aList[i] = NULL;
}

//Traverse Through Hardness Map And Add Edges
//Basically building our network of nodes.
for(j=0;j<MAX_V;j++){
	for(i = 0; i<=MAX_H;i++){
		int index = i+j*MAX_H;
		int cells[4];
		cells[0] = index-MAX_H;
		cells[1] = index+1;
		cells[2] = index+MAX_H;
		cells[3] = index-1;
		
		
		int n;
		for(n = 0;n<4;n++){
			if(cells[n]>= 0 && cells[n]<(MAX_H*MAX_V)){
				int x = cells[n]%MAX_H;
				int y = cells[n]/MAX_H;
				if(x>0 && x<MAX_H && y>0 && y<MAX_V)
				aList[index] = addEdge(aList[index],cells[n],map[x][y]);
			}			
		}
		
	}
}


vertex_t min;

int distances[total_vertex];
int parent[total_vertex];
vertex_t pQ[total_vertex];
int pos[total_vertex];



//Initial all routes.
for(i = 0; i<total_vertex;i++){
	distances[i] = INT_MAX;
	parent[i] = 0;
	pQ[i].dist = INT_MAX;
	pQ[i].vertex = i;
	pos[i] = i;
}

//Set Distance of Source to zero

distances[start] = 0;
pQ[start].dist = 0;
buildHeap(pQ,total_vertex,pos);

for(i = 0; i<total_vertex;i++){
	min = pop(pQ,total_vertex,pos);
	
	node_t * trav = aList[min.vertex];
	
	while(trav!=NULL){
		int u = min.vertex;
		int v = trav->vertex;
		int w = trav->weight;
		
		if(distances[u] != INT_MAX && distances[v] > distances[u]+w){
			//printf("%d, %d , %d \n", i, v, total_vertex);
			distances[v] = distances[u]+w;
			parent[v] = u;
			vertex_t changed;
			changed.vertex = v;
			changed.dist = distances[v];
			decreaseKey(pQ,changed,pos);
		}
		trav=trav->next;
		
	}
	
}



i = parent[end];
j = 0;


pList p;

while(start!=parent[i]){
	i=parent[i];
	p.List[j] = i;
	j++;
	p.size = j;
}


return p;
}
